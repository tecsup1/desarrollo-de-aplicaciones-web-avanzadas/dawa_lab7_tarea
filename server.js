const express = require("express");
const app = express();
const port = process.env.PORT || 3000;

app.use(express.static(__dirname + "/public"));

app.set("view engine", "pug");

var lab5_app = require("tecsup-tarea-quispefernandez"); // comprueba inputs de forms

var bodyParser = require("body-parser");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));



// instanciando paquete de express-validator:
const { check, validationResult } = require("express-validator");

// importando paquetes para lab 7
const mongoose = require("mongoose");
mongoose.connect("mongodb://localhost:27017/lab07DB", (err, res) => {
    if (err) throw err;
    console.log("Base de datos Online");
});


// INTRODUCIR RUTAS EN ESTA AREA:

app.get("/", function (req, res) {
    res.render("index", {});
});

app.get("/quienes_somos", function (req, res) {
    res.render("quienes_somos", {});
});

app.get("/productos", function (req, res) {
    res.render("productos", {});
});

app.get("/contactenos", function (req, res) {
    res.render("contactenos", {
        nombre: "Gonzalo",
    });
});

app.post("/handleForm", [
    check("name").isLength({ min: 4 }).withMessage('Su nombre completo debe tener más de 3 caracteres.'),
    check("email").isEmail().withMessage('Introduzca un email válido'),
    check("phone").isLength({min:9,max:9}).withMessage('El número de celular debe tener 9 dígitos.'),
    check("phone").isNumeric().withMessage('El número de celular debe contener solo números.'),
    check("message").isLength({ min: 150 }).withMessage('El mensaje debe tener más de 150 caracteres'),

], function (req,res
) {
    const name = req.body.name;
    const date = req.body.date;
    const phone = req.body.phone;
    const email = req.body.email;
    const message = req.body.message;

    //console.log(name, date, phone, email, message);

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        //return res.status(422).json({ errors: errors.array() });
        //{"errors":[{"value":"","msg":"Invalid value","param":"name","location":"body"}]}
        // {"errors":[{"value":"gon","msg":"Debe tener más de 3 caracteres.","param":"name","location":"body"},{"value":"qwer","msg":"Introduzca un email válido","param":"email","location":"body"}]}
        //console.log(errors.array());
        res.render("contactenos", {
            errors: errors.array(),
        });
    } else {
        res.render("confirmation", {
            'name': name,
            'date':date,
            'phone':phone,
            'email':email,
            'message':message,
        });
    }
});

// rutas del archivo lab7routes.js:
require('./lab7/routes/lab7routes')(app);


app.listen(port, () =>
    console.log(`Escuchando peticiones en el puerto ${port}`)
);
